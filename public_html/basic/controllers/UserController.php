<?php

namespace app\controllers;

use Yii;
use app\models\User;
use yii\web\Controller;


/**
 * DemoController implements the CRUD actions for Demo model.
 */
class UserController extends Controller
{
    public function actionIndex()
    {
        return $this->goHome();	
    }

    public function actionAdd()
    {
		$user = new User();
		$user->username = "admin";
		$user->password = "admin";
		$user->save();
		$user = new User();
		$user->username = "user";
		$user->password = "user";
		$user->save();		
		return $this->goHome();		
    }
}