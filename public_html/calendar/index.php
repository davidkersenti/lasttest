<?php

require __DIR__ . '/vendor/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfigFile('client_secrets.json');
$client->addScope('https://www.googleapis.com/auth/calendar');

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  
  $client->setAccessToken($_SESSION['access_token']);
  $service = new Google_Service_Calendar($client);

  $event = new Google_Service_Calendar_Event(array(
  'summary' => 'Hapoel Jerusalem VS Maccabi Tel Aviv',
  'location' => 'Arena',
  'description' => 'Going to fuck maccabi',
  'start' => array(
    'dateTime' => '2017-02-15T16:00:00-17:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  'end' => array(
    'dateTime' => '2017-04-15T05:00:00-07:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  'recurrence' => array(
    'RRULE:FREQ=DAILY;COUNT=2'
  ),
   'participants' => array(
    array('email' => 'davidkersenti@gmail.com'),
    array('email' => 'eliaelbaz10@gmail.com'),
  ),
  'attendees' => array(
    array('email' => 'davidkersenti@gmail.com'),
    array('email' => 'eliaelbaz10@gmail.com'),
  ),
  'reminders' => array(
    'useDefault' => FALSE,
    'overrides' => array(
      array('method' => 'email', 'minutes' => 24 * 60),
      array('method' => 'popup', 'minutes' => 10),
    ),
  ),
));
  
$calendarId = 'primary';
$event = $service->events->insert($calendarId, $event);
printf('Event created: %s\n', $event->htmlLink);
  
} else {
  $redirect_uri =  'http://davidke.myweb.jce.ac.il/calendar/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));

}


