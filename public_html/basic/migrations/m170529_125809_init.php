<?php

use yii\db\Migration;

class m170529_125809_init extends Migration
{
       public function up()
    {
		$this->createTable(

		'user',
			[
				'id' => 'pk',
				'username' => 'string',
				'password' => 'string',
				'auth_key' => 'string'
			]
		);
   }

    public function down()
    {
        echo "m170529_125809_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
